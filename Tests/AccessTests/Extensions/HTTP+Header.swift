import Foundation
@testable import Access

extension HTTP.Header {
    var rawValue: [String: String] { [key: value] }
}
