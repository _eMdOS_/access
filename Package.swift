// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "Access",
    platforms: [.iOS(.v9), .tvOS(.v9), .macOS(.v10_15)],
    products: [
        .library(
            name: "Access",
            targets: ["Access"]
        )
    ],
    dependencies: [],
    targets: [
        .target(
            name: "Access",
            dependencies: []
        ),
        .testTarget(
            name: "AccessTests",
            dependencies: ["Access"]
        )
    ],
    swiftLanguageVersions: [.v5]
)
