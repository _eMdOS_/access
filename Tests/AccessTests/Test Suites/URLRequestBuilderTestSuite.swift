import XCTest
import Access

final class URLRequestBuilderTestSuite: XCTestCase {
    private struct TestData: Codable, Equatable {
        let value: String
    }
}

extension URLRequestBuilderTestSuite {
    func testURLRequest() throws {
        // given
        let url = URL(string: "https://api.test.com/test")!
        let httpMethod: HTTP.Method = .get
        let cachePolicy: URLRequest.CachePolicy = .useProtocolCachePolicy
        let timeout: TimeInterval = 60.0
        let headers: [HTTP.Header] = [
            .contentType(type: .application, subtype: .json),
            .userAgent("apple/package")
        ]
        let requestBody = TestData(value: "test-value")

        // when
        let urlRequest = URLRequest
            .Builder(url: url)
            .setMethod(httpMethod)
            .setCachePolicy(cachePolicy)
            .setTimeout(timeout)
            .setHeaders(headers)
            .setBody(try JSONEncoder().encode(requestBody))
            .build()

        // then
        XCTAssertEqual(httpMethod.rawValue, urlRequest.httpMethod)
        XCTAssertEqual(cachePolicy, urlRequest.cachePolicy)
        XCTAssertEqual(timeout, urlRequest.timeoutInterval)
        XCTAssertEqual(headers.count, urlRequest.allHTTPHeaderFields?.count)
        XCTAssertEqual("application/json", urlRequest.allHTTPHeaderFields?["Content-Type"])
        XCTAssertEqual("apple/package", urlRequest.allHTTPHeaderFields?["User-Agent"])
        XCTAssertEqual(requestBody, try urlRequest.httpBody.map({ try JSONDecoder().decode(TestData.self, from: $0) }))
    }
}
