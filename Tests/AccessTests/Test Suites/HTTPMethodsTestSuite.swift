import XCTest
import Access

final class HTTPMethodsTestSuite: XCTestCase {
    func testMethods() {
        let methodsDictionary: [String: HTTP.Method] = [
            "GET": .get,
            "HEAD": .head,
            "POST": .post,
            "PUT": .put,
            "DELETE": .delete,
            "PATCH": .patch
        ]

        methodsDictionary
            .forEach {
                XCTAssertEqual($0.key, $0.value.rawValue)
            }
    }
}
