import Foundation

public typealias EmptyBody = HTTP.Body.Empty
public typealias JSONBody<T: Encodable> = HTTP.Body.JSON<T>

public protocol HTTPRequestBody {
    func data() throws -> Data
}

public extension HTTP {
    enum Body { /* namespacing */ }
}

// MARK: Empty

extension HTTP.Body {
    public struct Empty: Encodable, HTTPRequestBody {
        public init() {}
        public func data() throws -> Data { .init() }
    }
}

// MARK: JSON

extension HTTP.Body {
    public struct JSON<T: Encodable>: HTTPRequestBody {
        let body: T
        let encoder: JSONEncoder

        public init(body: T, encoder: JSONEncoder = .init()) {
            self.body = body
            self.encoder = encoder
        }

        public func data() throws -> Data {
            try encoder.encode(body)
        }
    }
}

// MARK: URLForm

public protocol URLFormConvertible {
    var urlEncoded: String { get }
}

extension HTTP.Body {
    public struct URLForm<T: URLFormConvertible>: HTTPRequestBody {
        let body: T

        public init(body: T) {
            self.body = body
        }

        public func data() throws -> Data {
            Data(body.urlEncoded.utf8)
        }
    }
}
