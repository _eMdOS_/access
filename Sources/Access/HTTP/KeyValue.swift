import Foundation

protocol KeyValue {
    var key: String { get }
    var value: String { get }
}
