import Foundation

extension String {
    var base64Encoded: String {
        Data(utf8).base64EncodedString()
    }
}
