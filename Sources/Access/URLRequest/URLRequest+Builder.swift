import Foundation

public extension URLRequest {
    final class Builder {
        private(set) var urlRequest: URLRequest

        /// Initializes the builder with the given `URL`.
        /// - Parameter url: url
        public init(url: URL) {
            self.urlRequest = .init(url: url)
        }
    }
}

public extension URLRequest.Builder {
    /// Sets the `cachePolicy` to use for the request.
    /// - Parameter cachePolicy: cachePolicy
    func setCachePolicy(_ cachePolicy: URLRequest.CachePolicy) -> Self {
        urlRequest.cachePolicy = cachePolicy
        return self
    }

    /// Sets the `timeout` for the request.
    /// - Parameter timeout: timeout
    func setTimeout(_ timeout: TimeInterval) -> Self {
        urlRequest.timeoutInterval = timeout
        return self
    }

    /// Sets the HTTP method to use.
    /// - Parameter method: HTTP method
    func setMethod(_ method: HTTP.Method) -> Self {
        urlRequest.httpMethod = method.rawValue
        return self
    }

    /// Sets the request's headers.
    /// - Parameter headers: headers
    func setHeaders(_ headers: [HTTP.Header]?) -> Self {
        guard let headers = headers else {
            urlRequest.allHTTPHeaderFields = nil
            return self
        }
        urlRequest.allHTTPHeaderFields = headers.reduce(into: [String: String]()) { $0[$1.key] = $1.value }
        return self
    }

    /// Sets the HTTP body.
    /// - Parameter httpBody: HTTP body
    func setBody(_ httpBody: Data?) -> Self {
        urlRequest.httpBody = httpBody
        return self
    }

    /// Returns the built `URLRequest`.
    func build() -> URLRequest {
        urlRequest
    }
}
