import Foundation

/// The HTTP `Authorization` request header contains the credentials to authenticate a user agent with a server,
/// usually after the server has responded with a **401** `Unauthorized` status and the `WWW-Authenticate` header.
enum Authorization {
    /// The `Basic` HTTP authentication scheme, which transmits credentials as username/password pairs, encoded using `Base64`.
    case basic(username: String, password: String)
    /// This specification describes how to use bearer tokens in HTTP requests to access **OAuth 2.0** protected resources.
    case bearer(token: String)
}

extension Authorization: KeyValue {
    var key: String { "Authorization" }
    var value: String {
        switch self {
        case .basic(let username, let password):
            return "Basic \("\(username):\(password)".base64Encoded)"
        case .bearer(let token):
            return "Bearer \(token)"
        }
    }
}
