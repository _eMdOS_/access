import Foundation

/// The `Accept` request HTTP header advertises which content types, expressed as MIME types, the client is able to understand.
/// Using content negotiation, the server then selects one of the proposals, uses it and informs the client of its choice with the `Content-Type` response header.
struct Accept {
    let mediaType: MediaType
}

extension Accept: KeyValue {
    var key: String { "Accept" }
    var value: String {
        reduce(mime: "\(mediaType.mimeType.rawValue)/\(mediaType.mimeSubtype.rawValue)", with: mediaType.parameters)
    }
}
