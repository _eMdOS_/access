import Foundation

extension URL {
    public final class Builder {
        private(set) var components: URLComponents = .init()

        /// Initializes the builder. By default, `https` is set as scheme.
        public init(scheme: Scheme = .https) {
            components.scheme = scheme.rawValue
        }
    }
}

public extension URL.Builder {
    /// Sets the scheme subcomponent of the URL.
    /// - Parameter scheme: scheme
    func setScheme(_ scheme: URL.Scheme?) -> Self {
        components.scheme = scheme?.rawValue
        return self
    }

    /// Sets the host subcomponent.
    /// - Parameter host: host
    func setHost(_ host: String?) -> Self {
        components.host = host
        return self
    }

    /// Sets the port subcomponent.
    /// - Parameter port: port
    func setPort(_ port: Int?) -> Self {
        components.port = port
        return self
    }

    /// Sets the path subcomponent.
    /// - Parameter path: path
    func setPath(_ path: String) -> Self {
        components.path = path
        return self
    }

    /// Sets an array of  key-value pairs to be sent as query parameters.
    /// - Parameter queryItems: query items
    func setQuery(_ queryItems: [URLQueryItem]?) -> Self {
        guard let queryItems = queryItems else {
            components.queryItems = nil
            return self
        }
        components.queryItems = queryItems.filter { $0.value != nil }
        return self
    }

    /// Returns the built URL from the components set using the builder's functions.
    /// When the URL cannot be built, the functions throws a `.badURL` (`URLError`)
    func build() throws -> URL {
        guard let url = components.url else {
            throw URLError(.badURL)
        }
        return url
    }
}
