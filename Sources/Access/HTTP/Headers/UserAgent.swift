import Foundation

/// The `User-Agent` request header contains a characteristic string that allows the network protocol peers to identify the application type, operating system,
/// software vendor or software version of the requesting software user agent.
struct UserAgent {
    let value: String
}

extension UserAgent: KeyValue {
    var key: String { "User-Agent" }
}
