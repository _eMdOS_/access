import Foundation
import Combine

func makeURL(scheme: URL.Scheme, host: String, endpoint: Endpoint) throws -> URL {
    try URL.Builder(scheme: scheme)
        .setPort(endpoint.port)
        .setHost(host)
        .setPath(endpoint.path)
        .setQuery(endpoint.query)
        .build()
}

func makeURLRequest(url: URL, method: HTTP.Method, headers: [HTTP.Header]?, body: HTTPRequestBody) throws -> URLRequest {
    URLRequest.Builder(url: url)
        .setMethod(method)
        .setHeaders(headers)
        .setBody(try body.data())
        .build()
}

extension HTTP {
    final public class Client {
        let scheme: URL.Scheme
        let host: String
        let session: URLSession

        public init(scheme: URL.Scheme = .https, host: String, session: URLSession = .shared) {
            self.scheme = scheme
            self.host = host
            self.session = session
        }
    }
}

extension HTTP.Client {
    @available(iOS 13.0, *)
    func request<T>(
        endpoint: Endpoint,
        response: HTTP.Response<T>,
        method: HTTP.Method,
        body: HTTPRequestBody,
        headers: [HTTP.Header]?
    ) -> AnyPublisher<T, Error> {
        do {
            let url = try makeURL(scheme: scheme, host: host, endpoint: endpoint)
            let request = try makeURLRequest(url: url, method: method, headers: headers, body: body)
            return session
                .dataTaskPublisher(for: request)
                .subscribe(on: DispatchQueue.global())
                .map(\.data)
                .tryMap { try response.decode($0) }
                .receive(on: DispatchQueue.main)
                .eraseToAnyPublisher()
        } catch {
            return Fail(error: error).eraseToAnyPublisher()
        }
    }

    func request<T>(
        endpoint: Endpoint,
        response: HTTP.Response<T>,
        method: HTTP.Method,
        body: HTTPRequestBody,
        headers: [HTTP.Header]?,
        result: @escaping (Result<T, Error>) -> Void
    ) {
        do {
            let url = try makeURL(scheme: scheme, host: host, endpoint: endpoint)
            let request = try makeURLRequest(url: url, method: method, headers: headers, body: body)
            let task = session
                .dataTask(with: request) { (data, urlResponse, error) in
                    if let data = data {
                        do {
                            result(.success(try response.decode(data)))
                        } catch {
                            result(.failure(error))
                        }
                    } else if let error = error {
                        result(.failure(error))
                    }
                }
            task.resume()
        } catch {
            result(.failure(error))
        }
    }
}

// MARK: GET

public extension HTTP.Client {
    func get<T>(
        endpoint: Endpoint,
        response: HTTP.Response<T>,
        headers: [HTTP.Header]? = nil,
        result: @escaping (Result<T, Error>) -> Void
    ) {
        request(endpoint: endpoint, response: response, method: .get, body: HTTP.Body.Empty(), headers: headers, result: result)
    }

    @available(iOS 13.0, *)
    func get<T>(
        endpoint: Endpoint,
        response: HTTP.Response<T>,
        headers: [HTTP.Header]? = nil
    ) -> AnyPublisher<T, Error> {
        request(endpoint: endpoint, response: response, method: .get, body: HTTP.Body.Empty(), headers: headers)
    }
}

// MARK: POST

public extension HTTP.Client {
    func post<T>(
        endpoint: Endpoint,
        response: HTTP.Response<T>,
        body: HTTPRequestBody = HTTP.Body.Empty(),
        headers: [HTTP.Header]? = nil,
        result: @escaping (Result<T, Error>) -> Void
    ) {
        request(endpoint: endpoint, response: response, method: .post, body: body, headers: headers, result: result)
    }

    @available(iOS 13.0, *)
    func post<T>(
        endpoint: Endpoint,
        response: HTTP.Response<T>,
        body: HTTPRequestBody = HTTP.Body.Empty(),
        headers: [HTTP.Header]? = nil
    ) -> AnyPublisher<T, Error> {
        request(endpoint: endpoint, response: response, method: .post, body: body, headers: headers)
    }
}

