import Foundation

public struct Endpoint {
    public let port: Int?
    public let path: String
    public let query: [URLQueryItem]?

    public init(port: Int? = nil, path: String, query: [URLQueryItem]? = nil) {
        self.port = port
        self.path = path
        self.query = query
    }
}
