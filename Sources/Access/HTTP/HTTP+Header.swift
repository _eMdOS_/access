import Foundation

public extension HTTP {
    struct Header: KeyValue {
        let key: String
        let value: String

        public init(key: String, value: String) {
            self.key = key
            self.value = value
        }
    }
}

// MARK: - HTTP Headers

public extension HTTP.Header {
    // MARK: Accept

    static func accept(type: MIMEType, subtype: MIMESubType, parameters: [String: String] = [:]) -> HTTP.Header {
        let accept = Accept(mediaType: MediaType(mimeType: type, mimeSubtype: subtype, parameters: parameters))
        return HTTP.Header(key: accept.key, value: accept.value)
    }

    // MARK: Authorization

    static func authorization(username: String, password: String) -> HTTP.Header {
        let authorization = Authorization.basic(username: username, password: password)
        return HTTP.Header(key: authorization.key, value: authorization.value)
    }

    static func authorization(token: String) -> HTTP.Header {
        let authorization = Authorization.bearer(token: token)
        return HTTP.Header(key: authorization.key, value: authorization.value)
    }

    // MARK: Content-Type

    static func contentType(type: MIMEType, subtype: MIMESubType, parameters: [String: String] = [:]) -> HTTP.Header {
        let contentType = ContentType(mediaType: MediaType(mimeType: type, mimeSubtype: subtype, parameters: parameters))
        return HTTP.Header(key: contentType.key, value: contentType.value)
    }

    // MARK: Proxy-Authorization

    static func proxyAuthorization(username: String, password: String) -> HTTP.Header {
        let authorization = ProxyAuthorization.basic(username: username, password: password)
        return HTTP.Header(key: authorization.key, value: authorization.value)
    }

    static func proxyAuthorization(token: String) -> HTTP.Header {
        let authorization = ProxyAuthorization.bearer(token: token)
        return HTTP.Header(key: authorization.key, value: authorization.value)
    }

    // MARK: User-Agent

    static func userAgent(_ value: String) -> HTTP.Header {
        let userAgent = UserAgent(value: value)
        return HTTP.Header(key: userAgent.key, value: userAgent.value)
    }
}
