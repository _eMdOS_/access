import Foundation

/// The HTTP `Proxy-Authorization` request header contains the credentials to authenticate a user agent to a proxy server,
/// usually after the server has responded with a `407` `Proxy Authentication Required` status and the `Proxy-Authenticate` header.
enum ProxyAuthorization {
    /// The `Basic` HTTP authentication scheme, which transmits credentials as username/password pairs, encoded using `Base64`.
    case basic(username: String, password: String)
    /// This specification describes how to use bearer tokens in HTTP requests to access **OAuth 2.0** protected resources.
    case bearer(token: String)
}

extension ProxyAuthorization: KeyValue {
    var key: String { "Proxy-Authorization" }
    var value: String {
        switch self {
        case .basic(let username, let password):
            return "Basic \("\(username):\(password)".base64Encoded)"
        case .bearer(let token):
            return "Bearer \(token)"
        }
    }
}
