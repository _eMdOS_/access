import Foundation

/// The `Content-Type` entity header is used to indicate the media type of the resource.
/// In responses, a `Content-Type` header tells the client what the content type of the returned content actually is.
struct ContentType {
    let mediaType: MediaType
}

extension ContentType: KeyValue {
    var key: String { "Content-Type" }
    var value: String {
        reduce(mime: "\(mediaType.mimeType.rawValue)/\(mediaType.mimeSubtype.rawValue)", with: mediaType.parameters)
    }
}
