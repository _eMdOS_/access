import XCTest
import Access

final class URLBuilderTestSuite: XCTestCase {
    func testURL() throws {
        let url = try URL.Builder()
            .setScheme(.https)
            .setHost("api.test.com")
            .setPort(80)
            .setPath("/test")
            .setQuery([
                .init(name: "sort", value: "ascending"),
                .init(name: "page_size", value: "20")
            ])
            .build()

        XCTAssertEqual("https://api.test.com:80/test?sort=ascending&page_size=20", url.absoluteString)
    }

    func testURLError() throws {
        let builder = URL.Builder()
            .setScheme(.https)
            .setHost("api.test.com")
            .setPort(80)
            .setPath("test") // error -> wrong path component

        XCTAssertThrowsError(try builder.build())
    }
}
