import Foundation

public typealias EmptyResponse = HTTP.Response<Void>
public typealias JSONResponse<T: Decodable> = HTTP.Response<T>

extension HTTP {
    public struct Response<T> {
        let decode: (Data) throws -> T
        init(decode: @escaping (Data) throws -> T) {
            self.decode = decode
        }
    }
}

public extension HTTP.Response where T: Decodable {
    init(decoder: JSONDecoder = .init()) {
        self.init() { try decoder.decode(T.self, from: $0) }
    }
}

public extension HTTP.Response where T == Void {
    init() {
        self.init() { _ in }
    }
}
