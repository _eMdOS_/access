import Foundation

// MARK: functions

func reduce(mime: String, with parameters: [String: String]) -> String {
    parameters
        .reduce(into: mime) { $0 += "; \($1.key)=\($1.value)" }
}

// MARK: MIMEType

public enum MIMEType: String {
    case any = "*"
    case application
    case text
}

// MARK: MIMESubtype

public enum MIMESubType: String {
    case any = "*"
    case json
    case formURLEncoded = "x-www-form-urlencoded"
}

// MARK: MediaType

struct MediaType {
    let mimeType: MIMEType
    let mimeSubtype: MIMESubType
    let parameters: [String: String]
}
