import XCTest
import Access

final class HTTPHeadersTestSuite: XCTestCase {
    func testAccept() {
        let header: HTTP.Header = .accept(type: .any, subtype: .any)
        XCTAssertEqual(["Accept": "*/*"], header.rawValue)
    }

    func testAuthorization() {
        let basicAuthorization: HTTP.Header = .authorization(username: "username", password: "password")
        XCTAssertEqual(["Authorization": "Basic dXNlcm5hbWU6cGFzc3dvcmQ="], basicAuthorization.rawValue)

        let bearerAuthorization: HTTP.Header = .authorization(token: "token")
        XCTAssertEqual(["Authorization": "Bearer token"], bearerAuthorization.rawValue)
    }

    func testContentType() {
        let header: HTTP.Header = .contentType(type: .application, subtype: .json, parameters: ["charset": "utf-8"])
        XCTAssertEqual(["Content-Type": "application/json; charset=utf-8"], header.rawValue)
    }

    func testProxyAuthorization() {
        let basicAuthorization: HTTP.Header = .proxyAuthorization(username: "username", password: "password")
        XCTAssertEqual(["Proxy-Authorization": "Basic dXNlcm5hbWU6cGFzc3dvcmQ="], basicAuthorization.rawValue)

        let bearerAuthorization: HTTP.Header = .proxyAuthorization(token: "token")
        XCTAssertEqual(["Proxy-Authorization": "Bearer token"], bearerAuthorization.rawValue)
    }

    func testUserAgent() {
        let header: HTTP.Header = .userAgent("user-agent")
        XCTAssertEqual(["User-Agent": "user-agent"], header.rawValue)
    }
}
