import Foundation

import Foundation

public extension HTTP {
    /// Definition of the methods to be used by an HTTP request.
    ///
    /// **HTTP** defines a set of **request methods** to indicate the desired action to be performed for a given resource.
    /// Although they can also be nouns, these request methods are sometimes referred as HTTP verbs.
    /// Each of them implements a different semantic, but some common features are shared by a group of them:
    /// e.g. a request method can be safe, idempotent, or cacheable.
    enum Method: String {
        /// The `GET` method requests a representation of the specified resource. Requests using `GET` should only retrieve data.
        case get = "GET"
        /// The `HEAD` method asks for a response identical to that of a `GET` request, but without the response body.
        case head = "HEAD"
        /// The `POST` method is used to submit an entity to the specified resource, often causing a change in state or side effects on the server.
        case post = "POST"
        /// The `PUT` method replaces all current representations of the target resource with the request payload.
        case put = "PUT"
        /// The `DELETE` method deletes the specified resource.
        case delete = "DELETE"
        /// The `PATCH` method is used to apply partial modifications to a resource.
        case patch = "PATCH"
    }
}

