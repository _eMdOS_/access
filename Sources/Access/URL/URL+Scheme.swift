import Foundation

extension URL {
    public enum Scheme: String {
        case http, https
    }
}
